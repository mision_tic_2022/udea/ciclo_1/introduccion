'''
Desarollar una calculadora que realice operaciones entre dos número de:
*Suma (+)
*Resta (-)
*Multiplicación (*)
*División (/)
Nota: 
Debe permitir que el usuario ingrese los números y seleccionar el tipo de operación
'''
#Pedro
a = float(input('digite el número 1: '))
b = float(input('digite el número 2: '))
print(
    """Menú:
    1. Sumar
    2. Restar
    3. Multiplicar
    4. Dividir
""")
opcion = int(input('Digite la opción: '))
if(opcion == 1):
    print(f"{a} + {b} = ", a + b)
elif(opcion == 2):
    print(f"{a} - {b} = ", a - b)
elif(opcion == 3):
    print(f"{a} * {b} = ", a * b)
elif(opcion == 4):
    print(f"{a} / {b} = ", a / b)

else:
    print("opción inválida")


#Jorve Iván
try:
    num1 = float(input('Por favor ingrese el primer número: '))
    num2 = float(input('Por favor ingrese el segundo número: '))

    operador = input('Por favor ingrese indique la operación que quiere ejecutar, escribiendo cualquiera de las siguientes opciones (sumar, restar, multiplicar o dividir):')

    if operador == 'sumar':
        total = num1 + num2
        print('La suma de los números es %.1f' %total)
    elif operador == 'restar':
        total = num1 - num2
        print('La resta de los números es %.1f' %total)
    elif operador == 'multiplicar':
        total = num1 * num2
        print('La multiplicación de los números es %.1f' %total)
    elif operador == 'dividir':
        total = num1 / num2
        print('La división de los números es %.1f' %total)
    else:
        print("No selecciono un operador valido")

except:
    print('Debe ingresar un valor númerico.')