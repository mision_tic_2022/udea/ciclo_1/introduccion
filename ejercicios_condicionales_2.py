
persona_1 = 20
persona_1_pertenece_udea = False
persona_2 = 17
persona_2_pertenece_udea = True

if persona_1 >= 18 and persona_1_pertenece_udea:
    print('Persona 1 puede ingresar al club de mayores')
elif persona_1 < 18 and persona_1_pertenece_udea:
    print('Persona 1 puede ingresar al club de menores')
else:
    print('Persona 1 no puede ingresar a ningún club')

if persona_2 >= 18 and persona_2_pertenece_udea:
    print('Persona 2 puede ingresar al club de mayores')
elif persona_2 < 18 and persona_2_pertenece_udea:
    print('Persona 2 puede ingresar al club de menores')
else:
    print('Persona 2 no puede ingresar a ningún club')