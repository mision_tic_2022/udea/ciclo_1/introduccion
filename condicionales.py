'''
Clase  Condicionales
'''

#Proposiciones
#Variables
p: bool = True
q: bool = True
r: bool = False
z: bool = False

'''
Un solo igual (=) asigna un valor a una variable
Un doble igual (==) compara valor entre variables
'''

#---------------AND---------------

#Condicionales solo con if
if p and q  :
    print('Cumple la condición 1')

if (p == False) and q  :
    print('Cumple la condición 2')

#Condicionales con if else

if q and r :
    print('Cumple condición 3')
else:
    print('No cumple condición 3')
    
if (not p ) and r :
    print('Cumple Condición 4')
else:
    print('No cumple condición 4')

if not (p and r):
    print('Cumple condición 5')
else:
    print('No cumple condición 5')

#---------------OR---------------
if p or r :
    print('Cumple condición 6')
else:
    print('No cumple condición 6')

if not p or r or z or q :
    print('Cumple la condición 7')
else:
    print('No cumple la condición 7')

if not (not p or r or z or q) :
    print('Cumple la condición 7')
else:
    print('No cumple la condición 7')

if p and q and (not r) or (not z):
    print('Cumple la condición 8')
else:
    print('No cumple la condición 8')


'''
Utilizando números
'''
numero_1 = 10
numero_2 = 20
numero_3 = 30
numero_4 = 10

#Si numero_1 es igual a numero_3 entonces...
if numero_1 == numero_3 :
    print('numero_1 es igual a la variable número_3')
else:
    print('numero_1 NO es igual a la variable número_3')

#Si numero_1 es diferente a numero_3 entonces...
if numero_1 != numero_3 :
    print('numero_1 es diferente a la variable número_3')
else:
    print('numero_1 es igual a la variable número_3')
