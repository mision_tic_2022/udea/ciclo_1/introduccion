#Esto es un comentario de una sola linea
'''Este es un comentario 
de varias lineas'''
''' '''
#Esto es una variable

num1 = 5
num2 = 10
#Esta linea representa la operación básica entre dos variables 
suma = num1 + num2

#Mostrar mensaje en consola
print("Hola mundo")
#Mostrar suma
print('El resultado es ', suma)

'''
Definir variables con sus respectivos tipos de datos
'''
#String
nombre: str = "Andrés"
#Entero
numero_1: int = 0
#Float
numero_2: float = 3.1416
#Boolean
bandera_1: bool = True
bandera_2: bool = False



