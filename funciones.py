
# def -> Palabra reservada para crear una función

def miFuncion():
    print("Hola mundo desde mi una función")

# Funciones con parametros


def sumar_con_parametros(num_1: float, num_2: float, num_3: float):
    resultado = num_1 + num_2 + num_3
    print(resultado)

# Llamar una función
# miFuncion()
#sumar_con_parametros(10, 20, 30)
#sumar_con_parametros(40, 50, 60)
#sumar_con_parametros(10, 50, 90)


'''-----------------------------------------------------'''


def sumar(num_1, num_2):
    suma = num_1 + num_2
    # Retorna el valor de la operación
    return suma


'''
print( sumar(10, 20) )
resultado = sumar(20, 40)
resultado -= 10
print(resultado)
'''


def restar(num_1, num_2):
    return num_1-num_2


def pedir_datos():
    opc = 0
    while opc != 3:
        try:
            num_1 = float(input('Ingrese el primer número>> '))
            num_2 = float(input('Ingrese el segundo número>> '))

            opc = int(input('1-> Sumar \n2-> Restar\n3-> Salir\n >> '))
            if opc == 1:
                print( sumar(num_1, num_2) )
            elif opc == 2:
                print( restar(num_1, num_2) )
        except:
            print('Datos inválidos')

#Llamar la función principal
pedir_datos()
